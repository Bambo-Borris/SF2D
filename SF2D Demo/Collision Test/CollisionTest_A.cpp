#include "stdafx.h"

// stl
#include <array>

// engine
#include <SF2D/SF2D.h>
#include <SF2D/SystemBase.h>
#include <SF2D/Application.h>
#include <SF2D/AssetLoader/AssetLoader.h>
#include <SF2D/Utilities/DebugTools.h>

#include <SF2D/Components/BoxCollider.h>
#include <SF2D/Components/CircleCollider.h>
#include <SF2D/Components/PhysicsBody.h>
#include <string>
//external
#include "imgui-SFML.h"
#include <imgui.h>

#ifdef _DEBUG
// CRT 
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif

using namespace sf2d;
using namespace sf2d::input;
using namespace sf2d::comps;
using namespace sf2d::maths;

void processEvent(const sf::Event& e)
{
	ImGui::SFML::ProcessEvent(e);
}

// custom imgui component
class imguicomp : public ComponentBase
{
public:
	imguicomp(Entity* pEntity) : ComponentBase(pEntity) {}
	~imguicomp() = default;

	virtual InitStatus init() override
	{
		ImGui::SFML::Init(*Application::getApp()->getRenderWindow());
		std::function<void(void)> subLastDraw = std::bind(&imguicomp::draw, this);
		Application::getApp()->subscribeToEventQueue(processEvent);
		Application::getApp()->getRenderer()->subscribeLastDrawCallback(subLastDraw);
		m_bWasInitSuccessful = true;
		return InitStatus::Success;
	}

	virtual void cleanUp() override
	{
		ImGui::SFML::Shutdown();
	}

	// call to invoke imgui::begin
	void begin(const std::string& name)
	{
		if (!m_bBeginCalled)
		{
			ImGui::Begin(name.c_str());
			m_bBeginCalled = true;
		}
	}

	// call to invoke imgui::end
	void end()
	{
		if (!m_bEndCalled)
		{
			m_bEndCalled = true;
			ImGui::End();
		}
	}

	// Call to invoke imgui::sfml::update
	void update()
	{
		if (!m_bUpdateRun)
		{
			ImGui::SFML::Update(*Application::getApp()->getRenderWindow(), sf::seconds(1.0f / (float)(Application::getApp()->getGameFPS())));
			m_bUpdateRun = true;
		}
	}

private:

	void draw()
	{
		// If init is unsuccessful we'll avoid invoking render since this can force an abrupt halt 
		// instead we'll print that we're skipping an imgui render call 
		if (m_bWasInitSuccessful)
		{
			static bool hasBeenPrint = false;
			if (!hasBeenPrint)
			{
				//KPRINTF("Skipping imgui draw");
				hasBeenPrint = true;
			}
		}

		// If the update hasn't been run yet, we should totally avoid calling imgui draw
		// this is an applicating halting mistake to make.
		if (!m_bUpdateRun)
		{
			return;
		}


		// If begin was not called, do not try render. 
		// This is an application halting mistake to make.
		if (!m_bBeginCalled)
		{
			return;
		}

		// If end was not called, do not try render. 
		// This is an application halting mistake to make.
		if (!m_bEndCalled)
		{
			return;
		}

		ImGui::SFML::Render(*Application::getApp()->getRenderWindow());
		m_bUpdateRun = false;
		m_bBeginCalled = false;
		m_bEndCalled = false;
	}

	bool m_bUpdateRun = false;
	bool m_bWasInitSuccessful = false;
	bool m_bBeginCalled = false;
	bool m_bEndCalled = false;
};

class Box2DComp : public ComponentBase
{
public:

	Box2DComp(Entity* pEntity) :
		ComponentBase(pEntity)
	{

	}

	virtual InitStatus init() override
	{
		const Vec2f BOX_BOUNDS(20.0f, 20.0f);
		const Vec2f FLOOR_BOUNDS(KCAST(float, GET_APP()->getWindowSize().x), 50);
		Scene* const pScene = GET_SCENE();


		for (int32 i = 0; i < BOX_COUNT; ++i)
		{
			auto const testBox = pScene->addEntityToScene();
			m_pBox = testBox;
			testBox->addComponent(new Sprite(testBox, BOX_BOUNDS));
			auto& trans = *testBox->transform;

			const Vec2f RandPos(maths::RandFloat(0, 250), maths::RandFloat(0, 250));
			trans.setPosition(RandPos);
			trans.setOrigin(BOX_BOUNDS * 0.5f);
			trans.setRotation(maths::RandFloat(0, 359));
			auto collider = new BoxCollider(testBox, Vec2f(BOX_BOUNDS));
			//auto collider = new KCCircleCollider(testBox, BOX_BOUNDS.x / 2.0f);
			testBox->addComponent(collider);
			m_boxes.push_back(testBox);

			collider->subscribeCollisionCallback(&m_callback);
		}
		return InitStatus::Success;
	}

	virtual void onEnterScene() override
	{
		auto const boxTexture = ASSET().getTexture("8Ball");

		for (auto& box : m_boxes)
		{
			box->getComponent<Sprite>()->setTexture(boxTexture);
		}
	}

	virtual void tick() override
	{
		static bool bOpen = true;
		auto imgui = m_entity->getComponent<imguicomp>();
		imgui->update();
		imgui->begin("Box2D Testing");
		ImGui::Text("Position:");
		const std::string position = std::to_string(m_pBox->transform->getPosition().x) + " " + std::to_string(m_pBox->transform->getPosition().y);
		ImGui::Text(position.c_str());
		imgui->end();

		for (auto& b : m_boxes)
		{
			b->getComponent<Sprite>()->setColour(sf::Color::White);
			//b->getComponent<KCSprite>()->setColour(Colour::White);
		}

		m_boxes[0]->transform->setPosition(InputHandler::GetMouseWorldPosition());
		m_boxes[0]->getComponent<Sprite>()->setColour(sf::Color::White);
	}

private:
	const int32 BOX_COUNT = 20;

	Entity* m_pBox = nullptr;
	std::vector<Entity*> m_boxes;

	ColliderBaseCallback m_callback = [this](const CollisionDetectionData& collData)
	{

		collData.collidedWith->getComponent<Sprite>()->setColour(Colour::Green);
		//collData.entityB->getComponent<KCSprite>()->setColour(Colour::Green);
	};

};

class SystemTest : public sf2d::SystemBase
{
public:
	SystemTest()
		: sf2d::SystemBase("SystemTest")
	{
	}

	virtual sf2d::InitStatus init() override
	{
		return sf2d::InitStatus::Success;
	}

	virtual void onEnterScene() override
	{
	}

	virtual void onExitScene() override
	{
	}

	virtual void tick() override
	{
	}

	virtual void cleanup() override
	{
	}

private:


};

#ifndef _CONSOLE
#include <Windows.h>
int WINAPI WinMain(HINSTANCE hThisInstance, HINSTANCE hPrevInstance, LPSTR lpszArgument, int nCmdShow)
#else
int main(void)
#endif
{
	int32 i = sf::Texture::getMaximumSize();
	AppInit initApp(false);
	initApp.gameFps = 60;
	initApp.physicsFps = 60;
	initApp.width = 1024;//sf::VideoMode::getDesktopMode().width;
	initApp.height = 768; // sf::VideoMode::getDesktopMode().height;
	initApp.windowStyle = WindowStyle::Windowed_Fixed_Size;
	initApp.windowTitle = "Collision Module Check";
	if (StartupEngine(&initApp) != sf2d::InitStatus::Success)
	{
		assert(false);
		return 1;
	}

	spdlog::set_default_logger(GET_APP()->m_logger);

	//const int sceneWidth = KAssetLoader::getAssetLoader().getLevelMap(L"test_level")->width;
	//const int sceneHeight = KAssetLoader::getAssetLoader().getLevelMap(L"test_level")->height;
	auto app = Application::getApp();
	app->getSceneDirector().addScene(new Scene(std::string("Main_Scene"), Rectf(0, 0, (70 * 32), (40 * 32))));
	app->getSceneDirector().addScene(new Scene(std::string("Test_Scene"), Rectf(0, 0, (70 * 32), (40 * 32))));
	app->getSceneDirector().setStartScene("Main_Scene");
	auto sys = std::make_shared<SystemTest>();
	GET_SCENE()->addSystem(sys);
	auto entity = GET_SCENE()->addEntityToScene();
	entity->addComponent(new Box2DComp(entity));
	entity->addComponent(new imguicomp(entity));

	//DO STUFF WITH ENTITY HERE
	if (InitialiseSubmodules() != sf2d::InitStatus::Success)
	{
		assert(false);
		return 1;
	}

	RunApplication();

	ShutdownEngine();
	_CrtDumpMemoryLeaks();
	return 0;
}

