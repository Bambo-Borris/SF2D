#pragma once

#include <SF2D/Application.h>

// Player Paddle Controller
class PlayerPaddleController : 
	public sf2d::ComponentBase
{
public:
	PlayerPaddleController(sf2d::Entity* entity, const sf2d::Vec2f& paddleSize)
		: sf2d::ComponentBase(entity), PADDLE_SIZE(paddleSize)
	{
		assert(entity);
	}
	~PlayerPaddleController() = default;

	virtual sf2d::InitStatus init() override
	{
		m_entity->transform->setOrigin(PADDLE_SIZE * 0.5f);
		return sf2d::InitStatus::Success;
	}

	virtual void onEnterScene() override;
	virtual void tick() override;

	void reset();

private:
	const sf2d::Vec2f PADDLE_SIZE;
	const float MOVE_SPEED = 150.0f;

	void collisionCb(const sf2d::comps::CollisionDetectionData& data);
	sf2d::comps::ColliderBaseCallback m_cbFunc = [this](const sf2d::comps::CollisionDetectionData& data) -> void
	{
		collisionCb(data);
	};
};

