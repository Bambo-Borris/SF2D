#include "stdafx.h"

// stl
#include <array>

// engine
#include <SF2D/SF2D.h>
#include <SF2D/Application.h>
#include <SF2D/SystemBase.h>

#include <SF2D/Components/BoxCollider.h>
#include <SF2D/Components/CircleCollider.h>
#include <SF2D/Components/PhysicsBody.h>

#include <imgui.h>
#include "imgui-SFML.h"
#include "PlayerComponents.h"
#include "AIComponents.h"
#include "BallComponents.h"

#ifdef _DEBUG
// CRT 
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#else 
#include <SFML/Main.hpp>
#endif

void processEvent(const sf::Event& e)
{
	ImGui::SFML::ProcessEvent(e);
}

// custom imgui component
class imguicomp : public sf2d::ComponentBase
{
public:
	imguicomp(sf2d::Entity* pEntity) : sf2d::ComponentBase(pEntity) {}
	~imguicomp() = default;

	virtual sf2d::InitStatus init() override
	{
		ImGui::SFML::Init(*sf2d::Application::getApp()->getRenderWindow());
		std::function<void(void)> subLastDraw = std::bind(&imguicomp::draw, this);
		sf2d::Application::getApp()->subscribeToEventQueue(processEvent);
		sf2d::Application::getApp()->getRenderer()->subscribeLastDrawCallback(subLastDraw);
		m_bWasInitSuccessful = true;
		return sf2d::InitStatus::Success;
	}

	virtual void cleanUp() override
	{
		ImGui::SFML::Shutdown();
	}

	// call to invoke imgui::begin
	void begin(const std::string& name)
	{
		if (!m_bBeginCalled)
		{
			ImGui::Begin(name.c_str());
			m_bBeginCalled = true;
		}
	}

	// call to invoke imgui::end
	void end()
	{
		if (!m_bEndCalled)
		{
			m_bEndCalled = true;
			ImGui::End();
		}
	}

	// Call to invoke imgui::sfml::update
	void update()
	{
		if (!m_bUpdateRun)
		{
			ImGui::SFML::Update(*GET_APP()->getRenderWindow(), sf::seconds(1.0f / (float)(GET_APP()->getGameFPS())));
			m_bUpdateRun = true;
		}
	}

private:

	void draw()
	{
		// If init is unsuccessful we'll avoid invoking render since this can force an abrupt halt 
		// instead we'll print that we're skipping an imgui render call 
		if (m_bWasInitSuccessful)
		{
			static bool hasBeenPrint = false;
			if (!hasBeenPrint)
			{
				//KPRINTF("Skipping imgui draw");
				hasBeenPrint = true;
			}
		}

		// If the update hasn't been run yet, we should totally avoid calling imgui draw
		// this is an applicating halting mistake to make.
		if (!m_bUpdateRun)
		{
			return;
		}


		// If begin was not called, do not try render. 
		// This is an application halting mistake to make.
		if (!m_bBeginCalled)
		{
			return;
		}

		// If end was not called, do not try render. 
		// This is an application halting mistake to make.
		if (!m_bEndCalled)
		{
			return;
		}

		ImGui::SFML::Render(*GET_APP()->getRenderWindow());
		m_bUpdateRun = false;
		m_bBeginCalled = false;
		m_bEndCalled = false;
	}

	bool m_bUpdateRun = false;
	bool m_bWasInitSuccessful = false;
	bool m_bBeginCalled = false;
	bool m_bEndCalled = false;
};



// Game Manager 
class GameManager : public sf2d::SystemBase
{
public:
	GameManager()
		: sf2d::SystemBase("GameManager")
	{

	}
	~GameManager() = default;

	virtual sf2d::InitStatus init() override;
	virtual void onEnterScene() override {}
	virtual void onExitScene() override {}
	virtual void tick() override;
	virtual void cleanup() override {}

private:
	void resetGameState();

	const sf2d::Vec2f PADDLE_SIZE = sf2d::Vec2f(64.0f, 512.0f);

	sf2d::Entity* m_playerPaddle = nullptr;
	sf2d::Entity* m_aiPaddle = nullptr;
	sf2d::Entity* m_ball = nullptr;
	sf2d::Entity* m_god = nullptr;
	std::array<sf2d::Entity*, 4> m_walls;
};

sf2d::InitStatus GameManager::init()
{
	m_god = GET_SCENE()->addEntityToScene();
	m_god->addComponent(new imguicomp(m_god));


	m_playerPaddle = GET_SCENE()->addEntityToScene();
	m_playerPaddle->setTag("Player Paddle");
	m_playerPaddle->addComponent(new sf2d::comps::Sprite(m_playerPaddle));
	m_playerPaddle->addComponent(new PlayerPaddleController(m_playerPaddle, PADDLE_SIZE));
	m_playerPaddle->addComponent(new sf2d::comps::BoxCollider(m_playerPaddle, PADDLE_SIZE));

	m_aiPaddle = GET_SCENE()->addEntityToScene();
	m_aiPaddle->setTag("AI Paddle");
	m_aiPaddle->addComponent(new sf2d::comps::Sprite(m_aiPaddle));
	m_aiPaddle->addComponent(new AIPaddleController(m_aiPaddle, PADDLE_SIZE));
	m_aiPaddle->addComponent(new sf2d::comps::BoxCollider(m_aiPaddle, PADDLE_SIZE));


	m_ball = GET_SCENE()->addEntityToScene();
	m_ball->setTag("Ball");
	m_ball->addComponent(new BallShape(m_ball, 64.0f));
	m_ball->addComponent(new BallController(m_ball, 64.0f));
	m_ball->addComponent(new sf2d::comps::CircleCollider(m_ball, 64.0f));


	const auto WindowSize = sf2d::Vec2f(GET_APP()->getWindowSize());

	for (sf2d::uint64 i = 0; i < 4; ++i)
	{
		m_walls[i] = GET_SCENE()->addEntityToScene();
		if (!m_walls[i])
		{
			assert(false);
			return sf2d::InitStatus::Nullptr;
		}
		switch (i)
		{
		case 0: // Left wall
		{
			const auto Size = sf2d::Vec2f{ 1.0f, WindowSize.x };
			m_walls[i]->addComponent(new sf2d::comps::BoxCollider(m_walls[i], Size));
			m_walls[i]->transform->setOrigin(Size * 0.5f);
			m_walls[i]->transform->setPosition(Size.x / 2.0f, WindowSize.y / 2.0f);
			m_walls[i]->setTag("LeftWall");
		}
		break;
		case 1: // Right wall
		{
			const auto Size = sf2d::Vec2f{ 1.0f, WindowSize.x };
			m_walls[i]->addComponent(new sf2d::comps::BoxCollider(m_walls[i], Size));
			m_walls[i]->transform->setOrigin(Size * 0.5f);
			m_walls[i]->transform->setPosition(sf2d::Vec2f(WindowSize.x - Size.x / 2.0f, WindowSize.y / 2.0f));
			m_walls[i]->setTag("RightWall");
		}
		break;
		case 2:
		{
			const auto Size = sf2d::Vec2f(WindowSize.x, 1.0f);
			m_walls[i]->addComponent(new sf2d::comps::BoxCollider(m_walls[i], Size));
			m_walls[i]->transform->setOrigin(Size * 0.5f);
			m_walls[i]->transform->setPosition(sf2d::Vec2f(WindowSize.x / 2.0f, Size.y / 2.0f));
			m_walls[i]->setTag("TopWall");
		}
		break;
		case 3:
		{
			const auto Size = sf2d::Vec2f(WindowSize.x, 1.0f);
			m_walls[i]->addComponent(new sf2d::comps::BoxCollider(m_walls[i], Size));
			m_walls[i]->transform->setOrigin(Size * 0.5f);
			m_walls[i]->transform->setPosition(sf2d::Vec2f(WindowSize.x / 2.0f, WindowSize.y - Size.y / 2.0f));
			m_walls[i]->setTag("BottomWall");
		}
		break;
		default:
			assert(false);
		}
	}

	return sf2d::InitStatus::Success;
}

void GameManager::tick()
{
	if (sf2d::input::InputHandler::JustPressed(sf::Keyboard::Escape))
	{
		GET_APP()->closeApplication();
	}

	if (sf2d::input::InputHandler::JustPressed(sf::Keyboard::R))
	{
		resetGameState();
	}
}

void GameManager::resetGameState()
{
	auto ppc = m_playerPaddle->getComponent<PlayerPaddleController>();
	assert(ppc);
	ppc->reset();

	auto aic = m_aiPaddle->getComponent<AIPaddleController>();
	assert(aic);
	aic->reset();

	auto ballController = m_ball->getComponent<BallController>();
	assert(ballController);
	ballController->reset();
}


sf2d::int32 main(sf2d::int32 argc, sf2d::int8* arv[])
{
	int32 i = sf::Texture::getMaximumSize();
	sf2d::AppInit initApp(false);
	initApp.gameFps = 60;
	initApp.physicsFps = 60;
	initApp.width = 1024;
	initApp.height = 768;
	initApp.windowStyle = sf2d::WindowStyle::Windowed_Fixed_Size;
	initApp.windowTitle = "SF2D Pong Demo";

	sf2d::StartupEngine(&initApp);

	auto app = sf2d::Application::getApp();
	auto scene = new sf2d::Scene(std::string("Main_Scene"), sf2d::Rectf(0, 0, (70 * 32), (40 * 32)));
	app->getSceneDirector().addScene(scene);
	app->getSceneDirector().setStartScene("Main_Scene");
	scene->addSystem(std::make_shared<GameManager>());

	spdlog::set_default_logger(GET_APP()->m_logger);

	//DO STUFF WITH ENTITY HERE
	sf2d::InitialiseSubmodules();

	sf2d::RunApplication();

	sf2d::ShutdownEngine();

	_CrtDumpMemoryLeaks();
	return 0;
}
