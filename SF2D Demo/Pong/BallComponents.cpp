#include "stdafx.h"	
#include "BallComponents.h"

BallShape::BallShape(sf2d::Entity* entity, float ballRadius)
	: sf2d::comps::RenderableBase(entity), m_radius(ballRadius)
{

}

sf2d::InitStatus BallShape::init()
{
	m_shape.setRadius(m_radius);

	return sf2d::InitStatus::Success;
}

void BallShape::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= m_entity->transform->getTransform();
	target.draw(m_shape, states);
}

// Ball Controller

BallController::BallController(sf2d::Entity* entity, float ballRadius)
	: sf2d::ComponentBase(entity), m_radius(ballRadius)
{

}

sf2d::InitStatus BallController::init()
{
	m_entity->transform->setOrigin(m_radius, m_radius);

	return sf2d::InitStatus::Success;
}

void BallController::onEnterScene()
{
	auto coll = m_entity->getComponent<sf2d::comps::ColliderBase>();
	coll->subscribeCollisionCallback(&m_cbFunc);

	reset();
}

void BallController::reset()
{
	const auto CentrePos = sf2d::Vec2f(GET_APP()->getWindowSize()) * 0.5f;
	m_entity->transform->setPosition(CentrePos);

	sf2d::int32 randomValue = sf2d::maths::RandInt(1, 100);
	if (randomValue > 50)
	{
		m_direction.x = 1.0f;
	}
	else
	{
		m_direction.x = -1.0f;
	}

	randomValue = sf2d::maths::RandInt(1, 100);
	if (randomValue > 50)
	{
		m_direction.y = 1.0f;
	}
	else
	{
		m_direction.y = -1.0f;
	}

	// Currently force it to go towards player while testing
	m_direction.x = -1.0f;
}

void BallController::tick()
{
	const auto dt = GET_APP()->getDeltaTime();
	m_entity->transform->move(m_direction * dt * BALL_SPEED);
	//auto p = sf2d::input::InputHandler::GetMouseWorldPosition();
	//m_entity->transform->setPosition(p);
}

void BallController::collisionCb(const sf2d::comps::CollisionDetectionData& data)
{
	spdlog::debug("Ball collided with {}", data.collidedWith->getTag());
	auto player = GET_SCENE()->findEntity("Player Paddle");
	assert(player);
	auto ai = GET_SCENE()->findEntity("AI Paddle");

	const auto resolve = [this, &data]() {
		m_entity->transform->move(data.penetration * data.collisionNormal);
	};

	if (data.collidedWith == player || data.collidedWith == ai)
	{
		m_direction.x *= -1;
		resolve();
	}
	else
	{
		if (data.collidedWith->getTag() == "LeftWall" || data.collidedWith->getTag() == "RightWall")
		{
			m_direction.x *= -1;
			resolve();
		}

		else if (data.collidedWith->getTag() == "TopWall" || data.collidedWith->getTag() == "BottomWall")
		{
			m_direction.y *= -1;
			resolve();
		}
	}
}