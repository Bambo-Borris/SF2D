#pragma once

#include <SF2D/Application.h>

// AI Paddle Controller
class AIPaddleController : public sf2d::ComponentBase
{
public:
	AIPaddleController(sf2d::Entity* entity, const sf2d::Vec2f& paddleSize)
		: sf2d::ComponentBase(entity), PADDLE_SIZE(paddleSize)
	{
		assert(entity);
	}
	~AIPaddleController() = default;

	virtual sf2d::InitStatus init() override
	{
		m_entity->transform->setOrigin(PADDLE_SIZE * 0.5f);
		return sf2d::InitStatus::Success;
	}

	virtual void onEnterScene() override;
	virtual void tick() override;

	void reset();


private:
	const sf2d::Vec2f PADDLE_SIZE;

};

