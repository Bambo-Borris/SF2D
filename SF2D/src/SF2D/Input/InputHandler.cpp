#include <SF2D/Input/InputHandler.h>
#include <SF2D/Application.h>

using namespace sf2d;
using namespace sf2d::input;

sf::RenderWindow* InputHandler::mp_window = nullptr;

std::set<sf::Keyboard::Key> InputHandler::m_keysJustPressed;
std::set<sf::Keyboard::Key> InputHandler::m_keysPressed;
std::set<sf::Keyboard::Key> InputHandler::m_keysJustReleased;
std::set<sf::Mouse::Button> InputHandler::m_mouseJustPressed;
std::set<sf::Mouse::Button> InputHandler::m_mousePressed;
std::set<sf::Mouse::Button> InputHandler::m_mouseJustReleased;
std::set<unsigned int> InputHandler::m_joystickJustPressed;
std::set<unsigned int> InputHandler::m_joystickJustReleased;
std::string InputHandler::m_textEntered;

sf::Vector2i InputHandler::m_mousePosition;
sf::Vector2i InputHandler::m_mouseDelta;
float InputHandler::m_mouseScrollDelta = 0.0f;
bool InputHandler::mb_mouseLocked = false;

void InputHandler::SetWindow(sf::RenderWindow* window)
{
	KCHECK(InputHandler::mp_window == nullptr);
	InputHandler::mp_window = window;
}

void InputHandler::Update()
{
	m_keysJustPressed.clear();
	m_keysJustReleased.clear();
	m_mouseJustPressed.clear();
	m_mouseJustReleased.clear();
	m_mouseScrollDelta = 0.0f;
	m_textEntered.clear();
	m_joystickJustPressed.clear();
	m_joystickJustReleased.clear();

	if (mp_window)
	{
		sf::Vector2i newMousePosition = sf::Mouse::getPosition(*mp_window);
		m_mouseDelta = newMousePosition - m_mousePosition;
		if (mb_mouseLocked)
		{
			m_mousePosition = sf::Vector2i(mp_window->getSize()) / 2;
			sf::Mouse::setPosition(m_mousePosition, *mp_window);
		}
		else
		{
			m_mousePosition = newMousePosition;
		}
	}
}

void InputHandler::HandleEvent(const sf::Event& evt)
{
	switch (evt.type)
	{
	case sf::Event::KeyPressed:
		EventKeyPressed(evt.key.code);
		break;
	case sf::Event::KeyReleased:
		EventKeyReleased(evt.key.code);
		break;
	case sf::Event::MouseButtonPressed:
		EventMouseButtonPressed(evt.mouseButton.button);
		break;
	case sf::Event::MouseButtonReleased:
		EventMouseButtonReleased(evt.mouseButton.button);
		break;
	case sf::Event::TextEntered:
		EventTextEntered(evt.text.unicode);
		break;
	case sf::Event::MouseWheelScrolled:
		EventMouseScrollMoved(evt.mouseWheelScroll.delta);
		break;
	case sf::Event::JoystickButtonPressed:
		EventJoystickButtonPressed(evt.joystickButton.button);
		break;
	case sf::Event::JoystickButtonReleased:
		EventJoystickButtonReleased(evt.joystickButton.button);
	default:
		//TODO log warning here.. 
		//LOG_WARNING(std::to_string(evt.type) + " is not a valid event type for KInput to handle");
		break;
	}
}

void InputHandler::EventKeyPressed(sf::Keyboard::Key key)
{
	m_keysJustPressed.insert(key);
	m_keysPressed.insert(key);
}

void InputHandler::EventKeyReleased(sf::Keyboard::Key key)
{
	auto removeIt = m_keysPressed.find(key);
	if (removeIt != m_keysPressed.end())
		m_keysPressed.erase(removeIt);
	m_keysJustReleased.insert(key);
}

void InputHandler::EventMouseButtonPressed(sf::Mouse::Button button)
{
	m_mouseJustPressed.insert(button);
	m_mousePressed.insert(button);
}

void InputHandler::EventMouseButtonReleased(sf::Mouse::Button button)
{
	auto removeIt = m_mousePressed.find(button);
	if (removeIt != m_mousePressed.end())
		m_mousePressed.erase(removeIt);
	m_mouseJustReleased.insert(button);
}

void InputHandler::EventMouseScrollMoved(float delta)
{
	m_mouseScrollDelta = delta;
}

void InputHandler::EventTextEntered(sf::Uint32 charCode)
{
	m_textEntered += (wchar_t)charCode;
}

void InputHandler::EventJoystickButtonPressed(unsigned int button)
{
	m_joystickJustPressed.insert(button);
}

void InputHandler::EventJoystickButtonReleased(unsigned int button)
{
	m_joystickJustReleased.insert(button);
}

bool InputHandler::JustPressed(sf::Keyboard::Key key)
{
	return ((m_keysJustPressed.find(key) != m_keysJustPressed.end()));
}

bool InputHandler::Pressed(sf::Keyboard::Key key)
{
	return (m_keysPressed.find(key) != m_keysPressed.end());
}

bool InputHandler::JustReleased(sf::Keyboard::Key key)
{
	return (m_keysJustReleased.find(key) != m_keysJustReleased.end());
}

bool InputHandler::MouseJustPressed(sf::Mouse::Button button)
{
	return (m_mouseJustPressed.find(button) != m_mouseJustPressed.end());
}

bool InputHandler::MousePressed(sf::Mouse::Button button)
{
	return (m_mousePressed.find(button) != m_mousePressed.end());
}

bool InputHandler::MouseJustReleased(sf::Mouse::Button button)
{
	return (m_mouseJustReleased.find(button) != m_mouseJustReleased.end());
}

bool InputHandler::JoystickJustPressed(unsigned int button)
{
	return ((m_joystickJustPressed.find(button) != m_joystickJustPressed.end()));
}

bool InputHandler::JoystickJustReleased(unsigned int button)
{
	return ((m_joystickJustReleased.find(button) != m_joystickJustReleased.end()));
}

void InputHandler::SetMouseLocked(bool mouseLocked)
{
	InputHandler::mb_mouseLocked = mouseLocked;
	if (mouseLocked)
	{
		m_mousePosition = sf::Vector2i(mp_window->getSize()) / 2;
		sf::Mouse::setPosition(m_mousePosition, *mp_window);
	}
}

Vec2f sf2d::input::InputHandler::GetMouseWorldPosition()
{
	//auto pRenderWindow = KApplication::getApp()->getRenderWindow();
	Vec2f worldPos = mp_window->mapPixelToCoords(m_mousePosition);

	return worldPos;
}