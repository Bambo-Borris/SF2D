#include <SF2D/Maths/OrientationData.h>

using namespace	sf2d;

OrientationData::OrientationData()
{
}

OrientationData::OrientationData(float angleInDeg)
{
	setOrientation(angleInDeg);
}

OrientationData::OrientationData(float mat00, float mat01, float mat10, float mat11)
	: m00(mat00), m01(mat01), m10(mat10), m11(mat11)
{

}

void sf2d::OrientationData::setOrientation(float angleInDeg)
{
	//convert user passed angle into radians
	const float theta = maths::Radians(angleInDeg);

	//Fill out rotation matrix
	m00 = cosf(theta); m01 = -sinf(theta);
	m10 = sinf(theta); m11 = cosf(theta);
}

OrientationData OrientationData::getAbsoluteOrientation() const
{
	return OrientationData
	(
		fabs(m00), fabs(m01),
		fabs(m10), fabs(m11)
	);
}

Vec2f OrientationData::getXAxis() const
{
	return Vec2f(m00, m10);
}

Vec2f OrientationData::getYAxis() const
{
	return Vec2f(m01, m11);
}

OrientationData OrientationData::getTransposedOrientation() const
{
	return OrientationData
	(
		m00, m10,
		m01, m11
	);
}

Vec2f OrientationData::operator*(const Vec2f & rhs) const
{
	return Vec2f
	(
		m00 * rhs.x + m01 * rhs.y, m10 * rhs.x + m11 * rhs.y
	);
}

SF2D_API OrientationData sf2d::OrientationData::operator*(const OrientationData & rhs) const
{
	// [00 01]  [00 01]
	// [10 11]  [10 11]
	return OrientationData
	(
		m[0][0] * rhs.m[0][0] + m[0][1] * rhs.m[1][0],
		m[0][0] * rhs.m[0][1] + m[0][1] * rhs.m[1][1],
		m[1][0] * rhs.m[0][0] + m[1][1] * rhs.m[1][0],
		m[1][0] * rhs.m[0][1] + m[1][1] * rhs.m[1][1]
	);
}


