#include <SF2D/Entity.h>
#include <SF2D/Components/Transform.h>

using namespace sf2d;
using namespace sf2d::comps;

Entity::Entity()
	: m_componentVector(1, nullptr), m_entityTag(GenerateUUID()), m_bIsActive(false), m_interactivity(EntitySceneInteractivity::Dynamic)
{
	m_componentVector[0] = new Transform(this);
	transform = dynamic_cast<Transform*>(m_componentVector[0]);
}


Entity::~Entity()
{
	for (ComponentBase* pComponent : m_componentVector)
	{
		pComponent->cleanUp();
		KFREE(pComponent);
	}
	m_componentVector.clear();
}

InitStatus Entity::init()
{
	for (int32 componentIdx = 0; componentIdx < (signed)m_componentVector.size(); ++componentIdx)
	{
		KCHECK(m_componentVector[componentIdx]);
		INIT_CHECK(m_componentVector[componentIdx]->init());
	}
	return InitStatus::Success;
}

void Entity::cleanUp()
{
	for (auto& pComp : m_componentVector)
	{
		KCHECK(pComp);

		pComp->cleanUp();
		KFREE(pComp);
	}
	m_componentVector.clear();
}

void Entity::tick()
{
	for (auto& pComp : m_componentVector)
	{
		KCHECK(pComp);
		pComp->tick();
	}
}

void Entity::fixedTick()
{
	for (auto& pComp : m_componentVector)
	{
		KCHECK(pComp);
		pComp->fixedTick();
	}
}

void Entity::onEnterScene()
{
	for (auto& pComp : m_componentVector)
	{
		KCHECK(pComp);
		pComp->onEnterScene();
	}
}

void Entity::onExitScene()
{
	for (auto& pComp : m_componentVector)
	{
		KCHECK(pComp);
		pComp->onExitScene();
	}
}

bool Entity::addComponent(ComponentBase* pComponent)
{
	KCHECK(pComponent);
	if (!pComponent)
	{
		return false;
	}

	m_componentVector.push_back(pComponent);
	return true;
}

bool Entity::removeComponent(ComponentBase* pComponent)
{
	auto findResult = std::find(m_componentVector.begin(), m_componentVector.end(), pComponent);

	if (findResult == m_componentVector.end())
	{
		return false;
	}
	m_componentVector.erase(findResult);
	return true;
}

void Entity::setEntityInteraction(EntitySceneInteractivity interactivity)
{
	m_interactivity = interactivity;
}

EntitySceneInteractivity Entity::getInteractivity() const
{
	return m_interactivity;
}