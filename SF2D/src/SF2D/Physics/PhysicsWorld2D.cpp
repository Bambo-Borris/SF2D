#include <SF2D/Physics/PhysicsWorld2D.h>
#include <SF2D/Physics/b2dConversion.h>

using namespace sf2d;
using namespace sf2d::physics;


PhysicsWorld2D::PhysicsWorld2D()
{

}

PhysicsWorld2D::~PhysicsWorld2D()
{
}

InitStatus PhysicsWorld2D::initialiseWorld()
{
	if (!m_pBox2DWorld)
	{
		m_pBox2DWorld = new b2World(Vec2fTob2(m_gravity));
	}

	return InitStatus::Success;
}

void PhysicsWorld2D::cleanupWorld()
{
	KFREE(m_pBox2DWorld);
}

void PhysicsWorld2D::stepWorld(float physicsStep)
{
	m_pBox2DWorld->Step(physicsStep, m_velocityIterations, m_positionIterations);
}

void PhysicsWorld2D::setGravity(const sf2d::Vec2f& g)
{
	m_gravity = g;
	m_pBox2DWorld->SetGravity(Vec2fTob2(g));
}

Vec2f PhysicsWorld2D::getGravity() const
{
	return m_gravity;
}

b2Body* PhysicsWorld2D::addNewBody(const b2BodyDef& def)
{
	return  m_pBox2DWorld->CreateBody(&def);
}

void PhysicsWorld2D::removeBody(b2Body* const pBody)
{
	m_pBox2DWorld->DestroyBody(pBody);
}

void PhysicsWorld2D::rayCast(const Vec2f& start, const Vec2f& end)
{
	RaycastCB cb;
	if (GetSquareLength(end - start) <= 0.0f)
		return;

	m_pBox2DWorld->RayCast(&cb, Vec2fTob2(start / m_ppm), Vec2fTob2(end / m_ppm));
}

void PhysicsWorld2D::setPPM(float ppm)
{
	//prevent division by 0
	if (ppm != 0.0f)
	{
		m_ppm = ppm;
	}
}