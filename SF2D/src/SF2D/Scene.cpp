#include <SF2D/Scene.h>
#include <SF2D/Application.h>
#include <SF2D/SystemBase.h>

#include <SF2D/Components/Transform.h>
#include <SF2D/Components/ColliderBase.h>

#include <SF2D/Components/TileMap.h>
#include <SF2D/Utilities/DebugTools.h>


using namespace sf2d;
using namespace sf2d::comps;

using namespace std;

// -- KSCENE -- \\

Scene::Scene(const std::string& sceneName, const Rectf& sceneBounds)
	: m_sceneName(sceneName), m_numberOfAllocatedChunks(0)
{

}

InitStatus Scene::initScene()
{
	// Init all systems
	for (auto& system : m_systems)
	{
		INIT_CHECK(system->init());
	}

	//initialisation pass => Initialise all entitys, and setup all components
	for (auto& chunk : m_entityChunks)
	{
		INIT_CHECK(chunk.entity.init()); // init components
		//TODO Move to onenter
		chunk.entity.getComponent<Transform>()->tick(); //tick transforms incase of transforms were applied during init of components
	}
	return InitStatus::Success;
}

void Scene::cleanUpScene()
{
	for (auto& system : m_systems)
	{
		system->cleanup();
	}

	for (auto& chunk : m_entityChunks)
	{
		chunk.entity.cleanUp();
	}
}

void Scene::tick()
{
	Application::getMutexInstance().lock();
	if (!m_hasTickedOnce)
	{
		m_hasTickedOnce = true;
	}

	for (auto& system : m_systems)
	{
		system->tick();
	}

	for (uint32 i = 0; i < CHUNK_POOL_SIZE; ++i)
	{
		if (!m_entityChunks[i].allocated) // if not allocated to the scene, ignore
		{
			continue;
		}
		if (!m_entityChunks[i].entity.isActive())
		{
			continue;
		}

		m_entityChunks[i].entity.tick(); // tick all components
	}
	Application::getMutexInstance().unlock();
}

void Scene::fixedTick()
{
	static std::stack<Entity*> colliderStack;
	static vector<pair<Entity*, Entity*>> alreadyCheckedCollisionPairs(500);

	//TODO remove mutex lock 
	Application::getMutexInstance().lock();
	for (uint32 i = 0; i < CHUNK_POOL_SIZE; ++i)
	{
		if (!m_entityChunks[i].allocated)
		{
			continue;
		}

		if (!m_entityChunks[i].entity.isActive())
		{
			continue;
		}
		m_entityChunks[i].entity.fixedTick();
	}

	Application::getMutexInstance().unlock();
}

void Scene::onEnterScene()
{
	Application::getMutexInstance().lock();
	for (auto& system : m_systems)
	{
		system->onEnterScene();
	}

	for (auto& entityChunk : m_entityChunks)
	{
		if (!entityChunk.allocated)
		{
			continue;
		}

		entityChunk.entity.onEnterScene();
	}
	Application::getMutexInstance().unlock();
}

void Scene::onExitScene()
{
	for (auto& system : m_systems)
	{
		system->onExitScene();
	}

	for (auto& entityChunk : m_entityChunks)
	{
		if (!entityChunk.allocated)
		{
			continue;
		}
		entityChunk.entity.onExitScene();
	}
}

Entity* Scene::addEntityToScene()
{
	Entity* const pEntity = getAllocatableEntity();
	if (!pEntity)
	{
		return nullptr;
	}

	++m_numberOfAllocatedChunks;
	return pEntity;
}

SF2D_API bool Scene::addMultipleEntitiesToScene(uint32 numberToAllocate, vector<Entity*>& entityVec)
{
	const int32 total = getFreeChunkTotal();
	//if there isn't enough available memory, do not allocate
	if (total < (signed)numberToAllocate)
	{
		return false;
	}
	entityVec.resize(numberToAllocate);
	int32 count = 0;

	for (auto it = std::begin(m_entityChunks); it != std::end(m_entityChunks); ++it)
	{
		if (count >= (signed)numberToAllocate)
		{
			break;
		}
		if (!it->allocated)
		{
			it->allocated = true;
			it->entity.setActive(true);
			entityVec[count] = &it->entity;
			++count;
		}
	}

	return true;
}

void Scene::removeEntityFromScene(Entity* pEntityToRemove)
{
	KCHECK(pEntityToRemove);
	if (!pEntityToRemove)
	{
		return;
	}

	for (auto it = std::begin(m_entityChunks); it != std::end(m_entityChunks); ++it)
	{
		if (&it->entity == pEntityToRemove)
		{
			pEntityToRemove->cleanUp();
		}
	}
}

Entity* Scene::findEntity(const std::string& tag)
{
	auto find = std::find_if(std::begin(m_entityChunks), std::end(m_entityChunks), [&tag](const AllocatableChunk& chunk) -> bool
		{
			if (chunk.entity.getTag() == tag)
			{
				return true;
			}
			return false;
		});

	if (find == std::end(m_entityChunks))
	{
		return nullptr;
	}
	return &(*find).entity;
}

std::vector<Entity*> Scene::getAllocatedEntityList()
{
	std::vector<Entity*> out;
	for (auto& c : m_entityChunks)
	{
		if (!c.allocated)
		{
			continue;
		}
		out.push_back(&c.entity);
	}
	return out;
}

void sf2d::Scene::addSystem(std::shared_ptr<SystemBase> system)
{
	m_systems.push_back(system);
}

bool sf2d::Scene::removeSystem(std::shared_ptr<SystemBase> system)
{
	auto result = std::find(m_systems.begin(), m_systems.end(), system);

	if (result == m_systems.end())
	{
		return false;
	}

	m_systems.erase(result);
	return true;
}

std::optional<std::shared_ptr<SystemBase>> sf2d::Scene::getSystemByID(std::string_view id)
{
	auto result = std::find_if(m_systems.begin(), m_systems.end(), [&id](const std::shared_ptr<SystemBase>& sys) -> bool
		{
			if (sys->getID() != id)
			{
				return false;
			}
			return true;
		});

	if (result == m_systems.end())
	{
		return std::nullopt;
	}

	return *result;
}

//private
Entity* Scene::getAllocatableEntity()
{
	auto findResult = std::find_if(std::begin(m_entityChunks), std::end(m_entityChunks), [](AllocatableChunk& entity) -> bool
		{
			return !entity.allocated;
		});

	if (findResult == std::end(m_entityChunks))
	{
		return nullptr;
	}
	findResult->allocated = true;
	findResult->entity.setActive(true);
	return &findResult->entity;
}

int32 Scene::getFreeChunkTotal() const
{
	int32 count = 0;
	for (auto& chunk : m_entityChunks)
	{
		if (!chunk.allocated)
		{
			++count;
		}
	}

	return count;
}

//-- KSCENEDIRECTOR -- \\

SceneDirector::SceneDirector()
	: m_pCurrentScene(nullptr), m_pNextScene(nullptr)
{
}

InitStatus SceneDirector::initScenes()
{
	if (m_pCurrentScene == nullptr)
		m_pCurrentScene = m_scenes[0];

	for (auto& pScene : m_scenes)
	{
		INIT_CHECK(pScene->initScene());
	}

	KCHECK(m_scenes.size() > 0); // check we have scenes available

	if (m_scenes.size() == 0)
	{
		spdlog::error("Failed to initialise scene director! No scenes added");
		return InitStatus::Failure;
	}


	return InitStatus::Success;
}

void SceneDirector::cleanupScenes()
{
	for (auto& pScene : m_scenes)
	{
		pScene->cleanUpScene();
		KFREE(pScene);
	}
	m_scenes.clear();
}

void SceneDirector::tickActiveScene()
{
	// If we're transitioning between scenes
	// then close the last scene and exit
	// the next tick after this will invoke 
	// onEnterScene for the new scene
	if (m_bIsChangingScene)
	{
		KCHECK(m_pNextScene);
		m_pCurrentScene->onExitScene();
		m_pCurrentScene = m_pNextScene;
		m_bIsChangingScene = false;
		m_pNextScene = nullptr;
		GET_APP()->getOverlord()->triggerSceneCleanup();
		return;
	}

	// If this is the first tick for a scene
	// then we'll call onEnterScene() routine
	if (!m_pCurrentScene->hasSceneTickedOnce())
	{
		m_pCurrentScene->onEnterScene();
		GET_APP()->getOverlord()->triggerSceneConstruct();
	}

	m_pCurrentScene->tick();
}

void SceneDirector::fixedTickActiveScene()
{
	m_pCurrentScene->fixedTick();
}

void SceneDirector::setStartScene(const std::string& sceneName)
{
	auto const pScene = findSceneByName(sceneName);
	if (!pScene)
	{
		return;
	}

	m_pCurrentScene = pScene;
}

void SceneDirector::transitionToScene(const std::string& sceneName)
{
	auto const pScene = findSceneByName(sceneName);
	if (!pScene)
	{
		return;
	}
	m_bIsChangingScene = true;
	m_pNextScene = pScene;
}

int32 SceneDirector::addScene(Scene* pScene)
{
	KCHECK(pScene);
	if (!pScene)
	{
		return EXIT_FAILURE;
	}

	m_scenes.push_back(pScene);
	return EXIT_SUCCESS;
}

int32 SceneDirector::removeScene(Scene* pScene)
{
	auto findResult = std::find(m_scenes.begin(), m_scenes.end(), pScene);

	if (findResult == m_scenes.end())
	{
		return EXIT_FAILURE;
	}

	m_scenes.erase(findResult);
	return EXIT_SUCCESS;
}

Scene* const sf2d::SceneDirector::getSceneByName(const std::string& sceneName)
{
	auto result = std::find_if(m_scenes.begin(), m_scenes.end(), [sceneName](Scene* pScene)
		{
			return sceneName == pScene->getSceneName();
		});

	if (result == m_scenes.end())
	{
		return nullptr;
	}
	return *result;
}

Scene* SceneDirector::findSceneByName(const std::string& name) const
{
	auto findResult = std::find_if(m_scenes.begin(), m_scenes.end(), [&name](Scene* pScene) -> bool
		{
			return pScene->getSceneName() == name;
		});

	if (findResult == m_scenes.end())
	{
		return nullptr;
	}

	return *findResult;
}
