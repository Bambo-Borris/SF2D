#include <SF2D/SystemBase.h>
#include <SF2D/Application.h>

sf2d::SystemBase::SystemBase()
	: m_id(sf2d::GenerateUUID()), m_currentScene(*GET_SCENE())
{
}

sf2d::SystemBase::SystemBase(std::string_view id)
	: m_id(id), m_currentScene(*GET_SCENE())
{
}

sf2d::SystemBase::SystemBase(std::string_view id, sf2d::Scene& scene)
	: m_id(id), m_currentScene(scene)
{
}

SF2D_API std::string_view sf2d::SystemBase::getID() const
{
	return m_id;
}
