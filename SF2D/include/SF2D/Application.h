#pragma once 

#include <SF2D/SF2D.h>
#include <SF2D/Render/Renderer.h>
#include <SF2D/Input/InputHandler.h>
#include <SF2D/Scene.h>
#include <SF2D/Physics/PhysicsWorld2D.h>
#include <SF2D/Collisions/CollisionOverlord.h>

#include <fstream>
#include <SFML/Graphics/RenderWindow.hpp> 
#include <SFML/System/Clock.hpp> 

#include <mutex>
#include <atomic>
#include <functional>

//Lowest FPS for Physics & Game: 24
//Highest FPS for Physics & Game: 80

#define GET_SCENE() sf2d::Application::getApp()->getCurrentScene()
#define PROFILING_ENABLED 0

namespace sf2d
{
	enum class WindowStyle : uint8
	{
		Windowed_Fixed_Size,
		Windowed_Resizeable,
		Fullscreen
	};

	struct AppInit
	{
		SF2D_API AppInit(bool loadPreset = false)
		{
			if (loadPreset)
				loadFromEnginePreset();
		}

		uint32 width = 0; // Width of the window 
		uint32 height = 0; // Height of the window
		uint32 gameFps = 60; // Game FPS (render & update)
		uint32 physicsFps = 100; // fps for physics engine to update at
		std::string windowTitle; //title of the window
		bool consoleWindow = true; // Is the console window enabled on your build
		sf2d::WindowStyle windowStyle = sf2d::WindowStyle::Windowed_Fixed_Size; // Window style

		SF2D_API void loadFromEnginePreset();

	};
	SF2D_API std::ifstream& operator >> (std::ifstream& os, AppInit& data);

	class Application
	{
	public:

		SF2D_API static Application* const getApp()
		{
			static Application* pApplication = new Application();

			return pApplication;
		}

		SF2D_API ~Application() = default;

		InitStatus initialiseScenes();

		SF2D_API void setupApplication(const AppInit& appInit);
		SF2D_API void runApplication();
		SF2D_API void cleanupApplication();

		//TODO do not expose this function in the dll
		SF2D_API sf::RenderWindow* const getRenderWindow() { return m_pRenderWindow; }
		SF2D_API render::Renderer* const getRenderer() { return &m_pRenderer; }
		SF2D_API SceneDirector& getSceneDirector() { return m_sceneDirector; }
		SF2D_API physics::PhysicsWorld2D& getPhysicsWorld() { return m_physicsWorld; }
		SF2D_API Scene* const getCurrentScene() { return m_sceneDirector.getCurrentScene(); }

		SF2D_API collisions::CollisionOverlord* getOverlord() { return &m_overlord; }

		SF2D_API float getElapsedTime() const;
		SF2D_API uint32 getGameFPS() const { return m_gameFPS; }
		SF2D_API float getDeltaTime() const { return m_gameDelta; }
		SF2D_API float getPhysicsDelta() const { return m_physicsDelta; }

		SF2D_API Vec2u getWindowSize() const;

		SF2D_API void closeApplication();

		SF2D_API void subscribeToEventQueue(std::function<void(const sf::Event&)> function);

		SF2D_API void setPrintFPS(bool bLogFPS) { m_bLogFPS = bLogFPS; }

		static std::mutex& getMutexInstance() { return s_mutex; }

		std::shared_ptr<spdlog::logger> m_logger;
	private:

		Application();

		void fixedStep();

		__forceinline void updateFrameTime(sf::Time& currentTime, sf::Time& lastTime, sf::Time& frameTime, sf::Time& accumulator);
		__forceinline void outputFPS(const sf::Time& currentTime, sf::Time& fpsLastTime);

		sf::RenderWindow* m_pRenderWindow = nullptr;
		render::Renderer m_pRenderer;
		SceneDirector m_sceneDirector;
		physics::PhysicsWorld2D m_physicsWorld;
		collisions::CollisionOverlord m_overlord;

		uint32 m_gameFPS;
		uint32 m_physicsFPS;

		int32 m_frames = 0;

		sf::Clock m_elapsedClock;

		float m_gameDelta = 0.0f;
		float m_physicsDelta = 0.0f;

		bool m_bIsFirstUpdate = true;
		bool m_bHasFocus = true;
		bool m_bLogFPS = true;
		Vec2f m_viewSize;
		static std::mutex s_mutex;
		std::vector<std::function<void(const sf::Event&)>> m_eventQueueCallbacks;
	};
}

#define GET_APP() sf2d::Application::getApp()
#define GET_DIRECTOR() sf2d::Application::getApp()->getSceneDirector()
