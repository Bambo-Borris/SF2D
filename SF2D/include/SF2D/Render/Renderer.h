#pragma once 

#include <SF2D/SF2D.h>
#include <SF2D/Components/Sprite.h>
#include <SF2D/Components/TileMap.h>
#include <SFML/Graphics/Text.hpp>	
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Shape.hpp>

#include <functional>

namespace sf2d
{
	namespace render
	{
		using TextRender = std::pair <Vec2i, sf::Text>;
		enum RendererType : uint32
		{
			Default,
			Raycast
		};

		enum class RenderSortType
		{
			None,
			LayerSort,
			ZOrderSort
		};

		class Renderer
		{
		public:

			SF2D_API Renderer();
			SF2D_API ~Renderer();

			SF2D_API void render();

			SF2D_API void setSortType(RenderSortType sortType) { m_sortType = sortType; }

			SF2D_API RenderSortType getSortType() const { return m_sortType; }

			SF2D_API KDEPRECATED_FUNC(int32 addTextToScreen)(const sf::Text& pText, const Vec2i& screenPos)
			{
				m_screenText.push_back(TextRender(screenPos, pText));
				m_screenText.back().second.setFont(mp_defaultFont);
				return (int32)m_screenText.size() - 1;
			}

			SF2D_API KDEPRECATED_FUNC(sf::Text& getTextByIndex)(int32 i) { return m_screenText[i].second; }

			SF2D_API void subscribeLastDrawCallback(std::function<void(void)> func); // subscribe a callback functions to be triggered as the last draw calls post entity drawing

			SF2D_API void addDebugShape(sf::Drawable* pShape) { m_debugShapes.push_back(pShape); }
			SF2D_API void removeDebugShape(sf::Drawable* pShape);

			SF2D_API void showDebugDrawables(bool bIsDrawingDebugShapes) { m_bShowDebugDrawables = bIsDrawingDebugShapes; }
			SF2D_API bool isShowingDebugDrawables() const { return m_bShowDebugDrawables; }

		private:

			void generateRenderableList();
			void sortByRenderLayer();
			void sortByZOrder();
			void defaultRender();

			Vec2f screenToWorld(const Vec2i& vec) const;

			std::vector<comps::RenderableBase*> m_renderablesVector;
			std::vector<comps::TileMapSplit*> m_splitMapVec;
			std::vector<TextRender> m_screenText;
			std::vector<std::function<void(void)>> m_lastDrawCallbacks;

			std::vector<sf::Drawable*> m_debugShapes;

			RendererType m_renderingType;
			RenderSortType m_sortType;

			bool m_bHasTiledMap = false;
			bool m_bShowDebugDrawables = false;

			sf::Font mp_defaultFont;

		};
	}
}
