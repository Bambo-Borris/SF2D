#pragma once

#include <SF2D/SF2D.h>
#include <string>
#include <map>
#include <vector>

#define MAX_PROPERTY_STRING_CHARS 3000

namespace sf2d
{
	namespace tiled
	{
		//TI denoting TiledImport
		enum class LayerTypes : int8
		{
			ObjectLayer,
			TileLayer
		};
		// @Remember to add a failed enum flag
		enum class PropertyTypes : int8
		{
			String,
			Int,
			Float,
			Bool,
			HexColour,
			File
		};

		enum class ObjectTypes : int8
		{
			Point,
			Rect,
			Circle,
			Polygon //@Remember Polygon not currently supported
		};

		union Property
		{
			char type_string[MAX_PROPERTY_STRING_CHARS]; //Files will also be stored as str
			int type_int;
			float type_float;
			bool type_bool;
			sf2d::Colour type_colour;
		};

		using PropertiesMap = std::map<std::string, Property>;
		using PropertyTypesMap = std::map<std::string, PropertyTypes>;

		struct MapBase
		{
			virtual ~MapBase() = default;
			std::string name;
			float x = 0.0f;
			float y = 0.0f;
			float width = 0.0f;
			float height = 0.0f;
			PropertiesMap propertiesMap;
			PropertyTypesMap propertyTypesMap;
		};

		struct Object : public MapBase
		{
			int32 gid = 0;
			int32 id = 0;
			float rotation = 0.0f; //degrees
			ObjectTypes objectType = ObjectTypes::Circle;
		};

		struct Layer : public MapBase
		{
			std::vector<Object> objectsVector;
			std::vector<sf2d::int32> tileData; //1D array of tile guid's
			std::string name;
			float offsetX;
			float offsetY;
			// width = 0 for object layer, grid width for tile layer
			// height = 0 for object layer, grid height for tile layer
			LayerTypes layerType;
		};

		struct Tileset : public MapBase
		{
			uint32 tileWidth = 0;
			uint32 tileHeight = 0;
			uint32 margin = 0;
			uint32 spacing = 0;
			uint32 firstGID = 0;
			uint32 tileCount = 0;
			std::map<std::string, PropertiesMap> tilePropertiesMap;
			std::map<std::string, PropertyTypesMap> tilePropertyTypesMap;
			//width & height represet image width & image height
		};

		struct MapRoot
		{
			uint32 height = 0;
			uint32 width = 0;
			uint32 tileWidth = 0;
			uint32 tileHeight = 0;
			uint32 nextObjectID = 0;

			std::string orientation;

			std::vector<Layer> layersVector;
			std::vector<Tileset> tilesetVector;

			PropertiesMap properties;
			PropertyTypesMap propertyTypes;

		};

		MapRoot* loadTiledJSONFile(const std::string filePath);

		void cleanupLevelMap(MapRoot* pMap); // @Remember Cleanup function implementation 
	}
}
