#pragma once 

#include <vector>
#include <codecvt>
#include <locale>
#include <string>
#include <SF2D/SF2D.h>
#include <SF2D/Component.h>

namespace sf2d
{
	/*Entity class which components are attached to.
	All entities automatically have a transform component attatched upon construction.*/
	namespace comps
	{
		class Transform;
	}

	//Entities default to Dynamic
	//Dynamic = Added to a Quadtree that is regenerated per-frame
	//Static = Added to Quadtree generated at initialise and never updated again
	enum EntitySceneInteractivity
	{
		Dynamic,
		Static
	};
	constexpr int32 CHUNK_POOL_SIZE{ 500 };

	class Entity
	{
	public:

		SF2D_API Entity();
		SF2D_API ~Entity();

		//Param: n/a 
		//Return: KInitStatus (success if init fine)
		//Info: initialise all components attached to an entity, if one retuns a failure entire init sequence will be halted
		SF2D_API InitStatus init();

		//Param: n/a 
		//Return: n/a
		//Info: Cleanup all components attached to an entity
		SF2D_API void cleanUp();

		//Param: n/a 
		//Return: n/a
		//Info: tick all components attached to an entity
		SF2D_API void tick();

		//Param: n/a 
		//Return: n/a
		//Info: fixed-tick all components attached to an entity
		SF2D_API void fixedTick();

		//Param: n/a 
		//Return: n/a
		//Info: call onEnterScene for all components attached to an entity, upon entering a new game scene
		SF2D_API void onEnterScene();

		//Param: n/a 
		//Return: n/a
		//Info: call onExitScene for all components attached to an entity, upon exiting a game scene
		SF2D_API void onExitScene();

		//Param: pointer to componenet to be added
		//Return: true if component is added, false if it wasn't
		//Info: 
		SF2D_API bool addComponent(ComponentBase* pComponent);

		//Param: pointer to componenet to be removed 
		//Return: true if component removed, false if it wasn't
		//Info: 
		SF2D_API bool removeComponent(ComponentBase* pComponent);

		//Param: string containing new tag
		//Return: n/a
		//Info: 
		SF2D_API void setTag(const std::string& tag) { m_entityTag = tag; }

		//Param: Type of component being requested
		//Return: Pointer to the component requested, if not found will return nullptr
		//Info: Query by component types uses a dynamic_cast<> to evaluate whether a pointer of the requested type is 
		//stored.
		template<typename TComponent>
		TComponent* const getComponent();


		template<typename TComponent>
		std::vector<TComponent*> getComponents();
		//Param: n/a 
		//Return: wstring tag of entity
		//Info:   
		SF2D_API const std::string& getTag() const { return m_entityTag; }

		//todo change to enabled
		SF2D_API bool isActive() const { return m_bIsActive; }

		SF2D_API void setActive(bool bActive) { m_bIsActive = bActive; }

		SF2D_API void setEntityInteraction(EntitySceneInteractivity interactivity);


		SF2D_API EntitySceneInteractivity getInteractivity() const;

		comps::Transform* transform = nullptr;

	private:
		std::vector<ComponentBase*> m_componentVector;
		std::string m_entityTag;
		bool m_bIsActive;
		EntitySceneInteractivity m_interactivity;
	};

	template<typename TComponent>
	inline TComponent* const Entity::getComponent()
	{
		const auto result = std::find_if(m_componentVector.begin(), m_componentVector.end(), [](ComponentBase* pBase) -> bool
			{
				return dynamic_cast<TComponent*>(pBase) != nullptr;
			});

		if (result == m_componentVector.end())
		{
			return nullptr;
		}
		return dynamic_cast<TComponent*>(*result);
	}

	template<typename TComponent>
	inline std::vector<TComponent*> Entity::getComponents()
	{
		std::vector<TComponent*> returnVector;

		
		for (auto& c : m_componentVector)
		{
			if (dynamic_cast<TComponent*>(c))
			{
				returnVector.push_back(dynamic_cast<TComponent*>(c));
			}
		}

		return returnVector;
	}
}
