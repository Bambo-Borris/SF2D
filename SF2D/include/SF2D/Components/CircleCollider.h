#pragma once

#include <SF2D/SF2D.h>
#include <SF2D/Components\ColliderBase.h>

class b2CircleShape;

namespace sf2d
{
	namespace comps
	{
		class Transform;
		class CircleCollider : public ColliderBase
		{
		public:

			SF2D_API CircleCollider(Entity* pEntity, float radius, bool bUseParentLocation = true);
			SF2D_API ~CircleCollider() = default;

			SF2D_API const Vec2f& getCentrePosition();
			SF2D_API float getRadius() const;
			SF2D_API virtual const Rectf& getBoundingBox() override;

		private:

			void updateCentrePosition();

			std::weak_ptr<b2CircleShape> m_pCircleShape;

			float m_radius;
			Vec2f m_centrePos;
			Rectf m_boundingBox;
			Transform* transform;
		};
	}
}
