#pragma once 

#include <SF2D/SF2D.h>
#include <SF2D/Components/Transform.h>
#include <SF2D/Components/RenderableBase.h>

#include <SFML/Graphics/VertexArray.hpp>

namespace sf2d
{
	namespace comps
	{
		class Sprite : public RenderableBase
		{
		public:

			SF2D_API Sprite(Entity* pEntity, const Vec2f& size = Vec2f(1.0f, 1.0f));
			SF2D_API ~Sprite() = default;

			SF2D_API virtual void draw(sf::RenderTarget& rTarget, sf::RenderStates rStates) const override;

			SF2D_API void setColour(const Colour& col);

			//Sets texture for this sprite 
			//(side effect of changing texture rect on use)
			SF2D_API void setTexture(sf::Texture* pTexture);
			SF2D_API void setTextureRect(const Recti& texRect);

			SF2D_API virtual Rectf getOnscreenBounds() const override;

			SF2D_API void operator = (const Sprite& spr);
			SF2D_API void setSize(const Vec2f& size);

		private:

			sf::VertexArray m_vertexArray;
			sf::Vector2f m_size;
			sf::Texture* m_pTexture;
			Transform* transform;

		};
	}
}
