#pragma once

#include <SF2D/SF2D.h>	
#include <SF2D/Component.h>
#include <SF2D/Entity.h>

#include <functional>
#include <vector>

#include <memory>

class b2Shape;
struct b2Transform;

namespace sf2d
{

	namespace comps
	{
		struct CollisionDetectionData
	{
		//KRAWLER_API bool operator == (const KCollisionDetectionData& rhs) const
		//{
		//	if (entityA != rhs.entityA && entityA != rhs.entityB)
		//	{
		//		return false;
		//	}

		//	if (entityB != rhs.entityB && entityB != rhs.entityA)
		//	{
		//		return false;
		//	}

		//	if (penetration != rhs.penetration)
		//	{
		//		return false;
		//	}

		//	if (collisionNormal != rhs.collisionNormal && collisionNormal != (rhs.collisionNormal * -1.0f))
		//	{
		//		return false;
		//	}
		//	return true;
		//}
		/*KEntity* entityA = nullptr;
		KEntity* entityB = nullptr;*/

		Entity* collidedWith = nullptr;

		float penetration = 0.0f;
		Vec2f collisionNormal = Vec2f(0.0f, 0.0f);
		uint32 contactCount = 0;
		Vec2f contacts[2];
	};

		enum class ColliderType : int32
		{
			AABB,
			Circle
			// Not currently supported
			//OBB, 
			//Polygon
		};

		struct ColliderFilteringData
		{
			int16 collisionFilter = 0x0001;
			int16 collisionMask = 0x0001;
		};

		using ColliderBaseCallback = std::function<void(const CollisionDetectionData& collData)>;

		class ColliderBase : public ComponentBase
		{
		public:

			SF2D_API ColliderBase(Entity* pEntity, ColliderType type, bool bUseParentLocation);
			SF2D_API virtual ~ColliderBase() = default;

			SF2D_API ColliderType getColliderType() const { return m_colliderType; }

			SF2D_API int32 subscribeCollisionCallback(ColliderBaseCallback* callback);

			SF2D_API int32 unsubscribeCollisionCallback(ColliderBaseCallback* callback);

			SF2D_API void collisionCallback(const CollisionDetectionData& collData);

			SF2D_API bool isCallbackSubscribed(ColliderBaseCallback* callback) const;

			SF2D_API virtual const Rectf& getBoundingBox() = 0;

			SF2D_API void setCollisionFilteringData(const ColliderFilteringData& filteringPOD);

			SF2D_API const ColliderFilteringData& getCollisionFilteringData() const { return m_filterData; }

			SF2D_API std::weak_ptr<b2Shape> getB2Shape() { return std::weak_ptr<b2Shape>(m_pShape); }

			b2Transform getB2Transform();

			SF2D_API void setUseParentLocation(bool bUseParentLoc)
			{
				m_bUseParentLocation = bUseParentLoc;
			}

			SF2D_API bool isUsingParentLocation() const {
				return m_bUseParentLocation;
			}
			//@REMEMBER Only valid if not using parent location, otherwise will be ignored
			SF2D_API virtual void setOverridePosition(const Vec2f& pos)
			{
				m_overridePosition = pos;
			}

			SF2D_API Vec2f getOverridePosition() const
			{
				return m_overridePosition;
			}

		private:

			std::vector<ColliderBaseCallback*> m_callbacks;

			uint16 m_collisionLayer;

			ColliderFilteringData m_filterData;
			ColliderType m_colliderType;

			std::shared_ptr<b2Shape> m_pShape;

			bool m_bUseParentLocation;
			Vec2f m_overridePosition; // For if not using parent location
		};
	}
}

