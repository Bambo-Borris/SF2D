#pragma once 

#include <SF2D/Components/RenderableBase.h>
#include <SF2D/Animation/Animation.h>
#include <SF2D/Components/Transform.h>
#include <SFML/Graphics/VertexArray.hpp>
#include <SF2D/SF2D.h>

namespace sf2d
{
	namespace comps
	{
		class AnimatedSprite : public RenderableBase
		{
		public:
			SF2D_API AnimatedSprite(sf2d::Entity *pEntity, anim::AnimationDef *pAnimation);
			SF2D_API ~AnimatedSprite() = default;

			SF2D_API virtual void draw(sf::RenderTarget &rTarget, sf::RenderStates rStates) const override;
			SF2D_API virtual void tick() override;

			SF2D_API virtual Rectf getOnscreenBounds() const override;

			SF2D_API void play();
			SF2D_API void pause();
			SF2D_API void stop();
			SF2D_API void setFrameIndex(sf2d::int32);

			SF2D_API void setAnimation(const std::string &animName, bool bUseOverrideSize = false);

			SF2D_API bool isRepeatingAnimation() const { return m_bRepeat; }
			SF2D_API void setRepeatingState(bool bRepeat) { m_bRepeat = bRepeat; }

			SF2D_API bool isPlaying() const { return m_bIsPlaying; }
			SF2D_API void setSize(const Vec2f &size);
			SF2D_API sf2d::Vec2f getSize() const { return m_size; }

			SF2D_API bool isPaused() const { return m_bIsPaused;}

		private:
			void updateTextureRect(const Rectf &texRect);
			void setupVertArray(bool bUseOverrideSize);

			anim::AnimationDef *m_pCurrentAnim = nullptr;
			Transform *m_pTransformComponent = nullptr;

			sf::VertexArray m_verts;

			float m_animTimer;
			sf2d::uint32 m_frameIdx;
			sf2d::Vec2f m_size;

			bool m_bIsPlaying;
			bool m_bIsPaused;
			bool m_bRepeat;
		};
	}
}

