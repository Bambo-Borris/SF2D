#pragma once 
#include <box2d/box2d.h>
#include <SF2D/SF2D.h>

// Utility Conversion Functions for Physics
sf2d::Vec2f b2ToVec2f(const b2Vec2& in);
b2Vec2 Vec2fTob2(const sf2d::Vec2f& in);
