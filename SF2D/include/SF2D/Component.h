#pragma once 

#include <SF2D/SF2D.h>

namespace sf2d
{
	//Forward declarations 
	class Entity;

	/*Component base class -- Top of all component hierachy.
	To create a custom component extend from this class.*/
	class ComponentBase
	{
	public:


		//Param: KEntity* 
		//Return: n/a
		//Info: Construct component with a pointer to entity it is attatched to 
		SF2D_API ComponentBase(Entity* pEntity);
		SF2D_API virtual ~ComponentBase();

		//Param: n/a
		//Return: KInit status for initialation result
		//Info: Init component (to be overridden if a component requires delayed initialisation)
		SF2D_API virtual InitStatus init();

		//Param: n/a
		//Return: KInit 
		//Info: Cleanup component (to be overridden if a component requires own cleanup routine)
		SF2D_API virtual void cleanUp();

		//Param: n/a
		//Return: n/a 
		//Info: Fixed tick component (to be overridden if a component interacts with the physics engine)
		SF2D_API virtual void fixedTick();

		//Param: n/a
		//Return: n/a 
		//Info: Tick component (to be overridden if a component updates)
		SF2D_API virtual void tick();

		//Param: n/a
		//Return: n/a 
		//Info: onEnterScene setup component (to be overridden if a component updates)
		SF2D_API virtual void onEnterScene();


		//Param: n/a
		//Return: n/a 
		//Info: onExitScene setup cleanup component (to be overridden if a component updates)
		SF2D_API virtual void onExitScene(); // TODO requires scene class!!

		//Param: wstring tag 
		//Return: n/a
		//Info: git
		SF2D_API void setComponentTag(const std::string& tag) { m_componentTag = tag; }

		//Param: n/a
		//Return: n/a 
		//Info: Tick component (to be overridden if a component updates)
		SF2D_API const std::string& getComponentTag() const { return m_componentTag; }

		//Param: n/a
		//Return: KEntity pointer 
		//Info: Allows derived access to a pointer to the component base
		Entity* const m_entity;

	private:

		std::string m_componentTag;
	};
}

