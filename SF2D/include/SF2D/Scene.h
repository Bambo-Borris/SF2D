#pragma once

#include <SF2D/SF2D.h>
#include <SF2D/Entity.h>

#include <SF2D/Utilities/KQuadtree.h>

#include <vector>
#include <list>
#include <optional>

namespace sf2d
{
	struct AllocatableChunk
	{
		bool allocated = 0;
		Entity entity;
	};

	namespace comps
	{
		class ColliderBase;
	}

	class SystemBase;

	class Scene
	{
	public:

		SF2D_API Scene(const std::string& sceneName, const Rectf& sceneBounds);
		SF2D_API ~Scene() = default;

		//override but call base impl before returning from derived 
		SF2D_API [[nodiscard]] virtual InitStatus initScene(); //init components

		//override but call base impl before returning from derived 
		SF2D_API void cleanUpScene();

		//override but call base impl before returning from derived 
		SF2D_API virtual void tick();

		//override but call base impl before returning from derived 
		SF2D_API virtual void fixedTick();

		//override but call base impl before returning from derived
		SF2D_API void onEnterScene();

		SF2D_API void onExitScene();

		SF2D_API [[maybe_unused]] const std::string& getSceneName() const { return m_sceneName; }

		//nullptr if none available or failed
		SF2D_API [[nodiscard]] Entity* addEntityToScene();

		SF2D_API [[nodiscard]] bool addMultipleEntitiesToScene(uint32 numberToAllocate, std::vector<Entity*>& entityVec);

		SF2D_API void removeEntityFromScene(Entity* pEntityToRemove);

		SF2D_API [[nodiscard]] Entity* findEntity(const std::string& tag);

		SF2D_API [[maybe_unused]] uint32 getNumbrOfEntitiesAllocated() const { return m_numberOfAllocatedChunks; }

		SF2D_API [[nodiscard]] std::vector<Entity*> getAllocatedEntityList();

		SF2D_API [[nodiscard]] AllocatableChunk* getEntityList() { return m_entityChunks; }

		SF2D_API void addSystem(std::shared_ptr<SystemBase> system);

		SF2D_API [[nodiscard]] bool removeSystem(std::shared_ptr<SystemBase> system);

		SF2D_API [[nodiscard]] std::optional<std::shared_ptr<SystemBase>> getSystemByID(std::string_view id);

		bool hasSceneTickedOnce() const { return m_hasTickedOnce; }

	private:

		sf2d::Entity* getAllocatableEntity();
		// Get the total number of unallocated chunks in the memory pool
		sf2d::int32 getFreeChunkTotal() const;
		bool m_hasTickedOnce = false;

		const int32 VelocityIterations = 6;
		const int32 PositionIterations = 2;

		AllocatableChunk m_entityChunks[CHUNK_POOL_SIZE];

		std::string m_sceneName;
		std::vector<comps::ColliderBase*> m_initCachedColliders;
		std::vector<std::shared_ptr<SystemBase>> m_systems;

		uint32 m_numberOfAllocatedChunks;
	};

	class SceneDirector
	{
	public:

		SF2D_API SceneDirector();
		SF2D_API ~SceneDirector() = default;

		SF2D_API [[nodiscard]] InitStatus initScenes();
		SF2D_API void cleanupScenes();

		SF2D_API void tickActiveScene();
		SF2D_API void fixedTickActiveScene();

		//@param Name of scene to start the application on
		SF2D_API void setStartScene(const std::string& sceneName);

		//@param Name of scene to transition to
		SF2D_API void transitionToScene(const std::string& sceneName);

		//@param Pointer to new scene
		SF2D_API [[nodiscard]] int32 addScene(Scene* pScene); //return 0 if added, EXIT_FAILURE if failed

		//@param pointer to scene to remove
		SF2D_API [[nodiscard]] int32 removeScene(Scene* pScene); //return 0 if removed, EXIT_FAILURE if failed

		SF2D_API [[nodiscard]] Scene* const getCurrentScene() { return m_pCurrentScene; }

		//@return returns pointer to scene or nullptr if no scene with name found
		SF2D_API [[nodiscard]] Scene* const getSceneByName(const std::string& sceneName);

	private:

		Scene* findSceneByName(const std::string& name) const;

		std::vector<Scene*> m_scenes;
		Scene* m_pCurrentScene;
		Scene* m_pNextScene;
		bool m_bIsChangingScene = false;
	};
}
